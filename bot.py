#!/usr/bin/python3.6
import socket,ssl,os,random
server="efnet.port80.se"
port=6697
botnick="Evangelist"
chan="#Christ"
bible_dir="bib"
color="\x03"
color_red="\x03"+"4"
color_blue="\x03"+"12"

def say(words):
    w = words.split(" ")
    if ":" in w[0]:
        bk=""
        v=w[0]
        r=" ".join(w[1:])
    else:
        bk=w[0]
        v=w[1]
        r=" ".join(w[2:])
    words = color_red +" "+ bk + color +" " + color_blue + v + color + " " + r

    ircs.send(bytes("PRIVMSG " + chan + " :"+ words + "\r\n", "UTF-8"))

def ping(seed):
    ircs.send(bytes("PONG :" + seed + "\r\n", "UTF-8"))
    print(">> PONG: " +seed)
    
def fetch_verse(verse):
    length=0
    l=[]
    i=-1
    try:
        book = verse[0].strip("\r\n")
        vers = verse[1].strip("\r\n")

        with open(bible_dir + "/" + book + ".txt") as f:
            text = f.readlines()
 
        
        if "-" in vers:
            nums =vers.split(":")[1]
            vers=vers.split("-")[0]
            length= int(nums.split("-")[1]) - int(nums.split("-")[0])
          
        for line in text:
            i=i+1
            if vers in line:
                if length > 0:
                    if length > 14:
                        length = 14
                        # x = " ".join(text[i:i+length+1])
                    x = text[i:i+length+1]
                    return(x)
                else:
                    l.append(line)
                    break

    except:
        l=['Bible verse not found!']
        

    return(l)

def seek_verse(seed):
    hits=[]
    seed=seed.lower()
    books = os.listdir(bible_dir)
    random.shuffle(books)
    for book in books:
        with open(bible_dir + "/" + book) as f:
            text = f.readlines()
        for line in text:
            if seed in line.lower().replace(",","").replace(";","").replace(":",""):
                h= book.replace(".txt","") + " " + line
                hits.append(h)

    if len(hits)<1:
        hit='"'+seed+'"' + ' was not found in the Bible!'
    else:
        hit=random.choice(hits)
        
    return(hit)       
            


def fetch_random():
    book=random.choice(os.listdir(bible_dir))
    with open(bible_dir + "/" + book) as f:
        lines = f.readlines()
    return (book.replace(".txt","") +" " +random.choice(lines).strip("\r\n"))

ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
ircs = ssl.wrap_socket(ircsock,ssl_version=ssl.PROTOCOL_TLSv1)
ircs.connect((server,port))
ircs.send(bytes("USER "+ botnick +" "+ botnick +" "+ botnick + " :" + botnick + "\r\n", "UTF-8")) 
ircs.send(bytes("NICK "+ botnick +"\r\n", "UTF-8"))
#ircs.send(bytes("JOIN "+ chan +"\r\n", "UTF-8"))

while 1:

    data = ircs.recv(2048).decode("UTF-8")
    if not data:
        break

    if "PING" in data[:4]:
        pong = data.split(":")[1]
        ping(pong)
    else:
        line = data.split("\r\n")
        for i in line:
           try:
               x = i.split(":",2)
               ircdata=             x[1] 
               ircmsg=    x[2]
               splitted = ircdata.split(" ")
               irccmd = splitted[1]
               if "!" in splitted[0]:
                   usernick = splitted[0].split("!",1)[0]
            
           except:
               continue
           
       
           if irccmd == "376":
               ircs.send(bytes("JOIN " + chan + "\r\n", "UTF-8"))
           
           elif irccmd == "PRIVMSG":
               if ircmsg[:1] == "!":
                   cmd = ircmsg[:2]
                   if cmd == "!b":
                       verse = ircmsg.split(" ",3)[1:3]
                       for i in fetch_verse(verse):
                           say(i.replace("\n",""))
                   elif cmd == "!r":
                       say(fetch_random())
                   elif cmd == "!s":
                       seed=ircmsg[3:].strip("\r\n")
                       z = seek_verse(seed)
                       say(z)
                       
                       
                       
                       
